﻿using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.DAL.Repositories;
using KnowledgeAccountingSystem.WEB.Controllers;
using KnowledgeAccountingSystem.WEB.ViewModels;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xunit;

namespace KnowledgeAccountingSystem.Test
{
    public class AdminControllerTests
    {
        [Fact]
        public void IndexViewDataMessage()
        {
            // Arrange
          
            IEnumerable<FieldOfKnowledgeDTO> fieldOfKnowledgeDTO = new List<FieldOfKnowledgeDTO>() 
            {
                new FieldOfKnowledgeDTO { Id = "06672286-dc9a-4e19-a39d-975efc18ea34", Name = "c#" }
            };
            var mock = new Mock<IAdminService>();
            var mock1 = new Mock<IAccountService>();
            var mock2 = new Mock<IUserService>();
            mock.Setup(repo => repo.GetAll()).Returns(fieldOfKnowledgeDTO);
            var controller = new AdminController(mock.Object, mock1.Object, mock2.Object);

            // Act
            var result = controller.Index();
            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable <IndexViewModels> >(viewResult.Model).FirstOrDefault();
            Assert.Equal("06672286-dc9a-4e19-a39d-975efc18ea34", model.Id);

        }
        
        [Fact]
        public void CreateMatirial()
        {
            FieldOfKnowledgeDTO fieldOfKnowledgeDTO = new FieldOfKnowledgeDTO { Id = "086f7ed5-b891-4c5c-9a08-dffc4a5b3f17", Name = "java" };
            IEnumerable<MatirialFieldOfKnowledgeDTO> matirial = new List<MatirialFieldOfKnowledgeDTO>()
            {
                new MatirialFieldOfKnowledgeDTO { Id= "267ee2e4-1d93-4e31-8250-da83ed829cac" , FieldOfKnowledge=fieldOfKnowledgeDTO, Name= "Linq" }
            };
            // Arrange
            var mock = new Mock<IAdminService>();
            var mock1 = new Mock<IAccountService>();
            var mock2 = new Mock<IUserService>();
            mock.Setup(repo => repo.CreateMatirial(new MatirialFieldOfKnowledgeDTO { Id= "267ee2e4-1d93-4e31-8250-da83ed829cac" , FieldOfKnowledgeId= "086f7ed5-b891-4c5c-9a08-dffc4a5b3f17" , Name= "Linq" }));
            mock.Setup(repo => repo.GetAllMaterial()).Returns(matirial);
            var controller = new AdminController(mock.Object, mock1.Object, mock2.Object);

            // Act
            controller.CreateMatirial(new CreateMatirialViewModels { Id = "267ee2e4-1d93-4e31-8250-da83ed829cac", FieldOfKnowledgeId = "086f7ed5-b891-4c5c-9a08-dffc4a5b3f17", Name = "Linq" });
            // Assert
            var result = controller.WorkWithMaterial();
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<WorkWithMaterialViewModels>>(viewResult.Model).FirstOrDefault(m=>m.Id== "267ee2e4-1d93-4e31-8250-da83ed829cac");
            Assert.Equal("267ee2e4-1d93-4e31-8250-da83ed829cac", model.Id);

        }
        
    }
}
