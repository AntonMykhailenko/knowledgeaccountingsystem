﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class FieldOfKnowledge
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        //public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }
        public ICollection<MatirialFieldOfKnowledge> MatirialFieldOfKnowledges { get; set; }
        public FieldOfKnowledge()
        {
           // ApplicationUsers = new List<ApplicationUser>();
            MatirialFieldOfKnowledges = new List<MatirialFieldOfKnowledge>();
        }

    }
}
