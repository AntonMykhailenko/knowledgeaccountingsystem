﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class MatirialFieldOfKnowledge
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string FieldOfKnowledgeId { get; set; } = Guid.NewGuid().ToString();
        public FieldOfKnowledge FieldOfKnowledge { get; set; }
        public ICollection<Mark> Marks { get; set; }
        public MatirialFieldOfKnowledge()
        {
            Marks = new List<Mark>();
        }
    }
}
