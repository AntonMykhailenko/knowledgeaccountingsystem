﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public ICollection<Mark> Marks { get; set; }
        public ApplicationUser()
        {
            Marks = new List<Mark>();
        }

        /*
        public virtual ICollection<FieldOfKnowledge> FieldOfKnowledges { get; set; }
        public ApplicationUser()
        {
            FieldOfKnowledges = new List<FieldOfKnowledge>();
        }
        */
    }
}
