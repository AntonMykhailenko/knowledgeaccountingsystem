﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class Mark
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public int Score { get; set; }
        public string MatirialFieldOfKnowledgeId { get; set; } = Guid.NewGuid().ToString();
        public MatirialFieldOfKnowledge MatirialFieldOfKnowledge { get; set; }
        public string ApplicationUserId { get; set; } = Guid.NewGuid().ToString();
        public ApplicationUser ApplicationUser { get; set; }
    }
}
