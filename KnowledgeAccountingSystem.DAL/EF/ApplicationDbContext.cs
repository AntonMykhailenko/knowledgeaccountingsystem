﻿using KnowledgeAccountingSystem.DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.EF
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }     
           
        public DbSet<FieldOfKnowledge> FieldOfKnowledges { get; set; }
        public DbSet<MatirialFieldOfKnowledge> MatirialFieldOfKnowledges { get; set; }
        public DbSet<Mark> Marks { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

     }
}
