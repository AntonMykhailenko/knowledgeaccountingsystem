﻿namespace KnowledgeAccountingSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMarkMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Marks",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Score = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.MatirialFieldOfKnowledges", "MarkId", c => c.String(maxLength: 128));
            CreateIndex("dbo.MatirialFieldOfKnowledges", "MarkId");
            AddForeignKey("dbo.MatirialFieldOfKnowledges", "MarkId", "dbo.Marks", "Id");
            DropColumn("dbo.MatirialFieldOfKnowledges", "Mark");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MatirialFieldOfKnowledges", "Mark", c => c.Int(nullable: false));
            DropForeignKey("dbo.MatirialFieldOfKnowledges", "MarkId", "dbo.Marks");
            DropIndex("dbo.MatirialFieldOfKnowledges", new[] { "MarkId" });
            DropColumn("dbo.MatirialFieldOfKnowledges", "MarkId");
            DropTable("dbo.Marks");
        }
    }
}
