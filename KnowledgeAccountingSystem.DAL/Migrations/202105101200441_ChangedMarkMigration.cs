﻿namespace KnowledgeAccountingSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedMarkMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ApplicationUserFieldOfKnowledges", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ApplicationUserFieldOfKnowledges", "FieldOfKnowledge_Id", "dbo.FieldOfKnowledges");
            DropForeignKey("dbo.MatirialFieldOfKnowledges", "MarkId", "dbo.Marks");
            DropIndex("dbo.MatirialFieldOfKnowledges", new[] { "MarkId" });
            DropIndex("dbo.ApplicationUserFieldOfKnowledges", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.ApplicationUserFieldOfKnowledges", new[] { "FieldOfKnowledge_Id" });
            AddColumn("dbo.Marks", "MatirialFieldOfKnowledgeId", c => c.String(maxLength: 128));
            AddColumn("dbo.Marks", "ApplicationUserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Marks", "MatirialFieldOfKnowledgeId");
            CreateIndex("dbo.Marks", "ApplicationUserId");
            AddForeignKey("dbo.Marks", "ApplicationUserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Marks", "MatirialFieldOfKnowledgeId", "dbo.MatirialFieldOfKnowledges", "Id");
            DropColumn("dbo.MatirialFieldOfKnowledges", "MarkId");
            DropTable("dbo.ApplicationUserFieldOfKnowledges");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ApplicationUserFieldOfKnowledges",
                c => new
                    {
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        FieldOfKnowledge_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.ApplicationUser_Id, t.FieldOfKnowledge_Id });
            
            AddColumn("dbo.MatirialFieldOfKnowledges", "MarkId", c => c.String(maxLength: 128));
            DropForeignKey("dbo.Marks", "MatirialFieldOfKnowledgeId", "dbo.MatirialFieldOfKnowledges");
            DropForeignKey("dbo.Marks", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Marks", new[] { "ApplicationUserId" });
            DropIndex("dbo.Marks", new[] { "MatirialFieldOfKnowledgeId" });
            DropColumn("dbo.Marks", "ApplicationUserId");
            DropColumn("dbo.Marks", "MatirialFieldOfKnowledgeId");
            CreateIndex("dbo.ApplicationUserFieldOfKnowledges", "FieldOfKnowledge_Id");
            CreateIndex("dbo.ApplicationUserFieldOfKnowledges", "ApplicationUser_Id");
            CreateIndex("dbo.MatirialFieldOfKnowledges", "MarkId");
            AddForeignKey("dbo.MatirialFieldOfKnowledges", "MarkId", "dbo.Marks", "Id");
            AddForeignKey("dbo.ApplicationUserFieldOfKnowledges", "FieldOfKnowledge_Id", "dbo.FieldOfKnowledges", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ApplicationUserFieldOfKnowledges", "ApplicationUser_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
