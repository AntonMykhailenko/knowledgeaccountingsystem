﻿namespace KnowledgeAccountingSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddKnowledgeEntMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MatirialFieldOfKnowledgeFieldOfKnowledges", "MatirialFieldOfKnowledge_Id", "dbo.MatirialFieldOfKnowledges");
            DropForeignKey("dbo.MatirialFieldOfKnowledgeFieldOfKnowledges", "FieldOfKnowledge_Id", "dbo.FieldOfKnowledges");
            DropIndex("dbo.MatirialFieldOfKnowledgeFieldOfKnowledges", new[] { "MatirialFieldOfKnowledge_Id" });
            DropIndex("dbo.MatirialFieldOfKnowledgeFieldOfKnowledges", new[] { "FieldOfKnowledge_Id" });
            AddColumn("dbo.MatirialFieldOfKnowledges", "FieldOfKnowledgeId", c => c.String(maxLength: 128));
            CreateIndex("dbo.MatirialFieldOfKnowledges", "FieldOfKnowledgeId");
            AddForeignKey("dbo.MatirialFieldOfKnowledges", "FieldOfKnowledgeId", "dbo.FieldOfKnowledges", "Id");
            DropTable("dbo.MatirialFieldOfKnowledgeFieldOfKnowledges");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MatirialFieldOfKnowledgeFieldOfKnowledges",
                c => new
                    {
                        MatirialFieldOfKnowledge_Id = c.String(nullable: false, maxLength: 128),
                        FieldOfKnowledge_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.MatirialFieldOfKnowledge_Id, t.FieldOfKnowledge_Id });
            
            DropForeignKey("dbo.MatirialFieldOfKnowledges", "FieldOfKnowledgeId", "dbo.FieldOfKnowledges");
            DropIndex("dbo.MatirialFieldOfKnowledges", new[] { "FieldOfKnowledgeId" });
            DropColumn("dbo.MatirialFieldOfKnowledges", "FieldOfKnowledgeId");
            CreateIndex("dbo.MatirialFieldOfKnowledgeFieldOfKnowledges", "FieldOfKnowledge_Id");
            CreateIndex("dbo.MatirialFieldOfKnowledgeFieldOfKnowledges", "MatirialFieldOfKnowledge_Id");
            AddForeignKey("dbo.MatirialFieldOfKnowledgeFieldOfKnowledges", "FieldOfKnowledge_Id", "dbo.FieldOfKnowledges", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MatirialFieldOfKnowledgeFieldOfKnowledges", "MatirialFieldOfKnowledge_Id", "dbo.MatirialFieldOfKnowledges", "Id", cascadeDelete: true);
        }
    }
}
