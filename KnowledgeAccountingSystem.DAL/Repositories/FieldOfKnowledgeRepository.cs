﻿using KnowledgeAccountingSystem.DAL.EF;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class FieldOfKnowledgeRepository : IRepository<FieldOfKnowledge>
    {
        private ApplicationDbContext db;

        public FieldOfKnowledgeRepository(ApplicationDbContext context)
        {
            this.db = context;
        }

        public void Create(FieldOfKnowledge item)
        {
            db.FieldOfKnowledges.Add(item);
        }

        public void Delete(string id)
        {
            FieldOfKnowledge fieldOfKnowledge = db.FieldOfKnowledges.Include(x=>x.MatirialFieldOfKnowledges).FirstOrDefault(x=>x.Id==id);
            if (fieldOfKnowledge != null)
                db.FieldOfKnowledges.Remove(fieldOfKnowledge);
        }

        public FieldOfKnowledge Find(string id)
        {
            return db.FieldOfKnowledges.Find(id);
        }

        public FieldOfKnowledge Get(string id)
        {
            return db.FieldOfKnowledges.FirstOrDefault(m => m.Id == id);
        }

        public IEnumerable<FieldOfKnowledge> GetAll()
        {
            return db.FieldOfKnowledges.Include(x=>x.MatirialFieldOfKnowledges).ToList();
        }

        public IEnumerable<MatirialFieldOfKnowledge> GetAllMatirial(string id)
        {
            IEnumerable<FieldOfKnowledge> elem = db.FieldOfKnowledges.Include(c => c.MatirialFieldOfKnowledges).ToList();
            FieldOfKnowledge el = elem.FirstOrDefault(x => x.Id == id);
            return el.MatirialFieldOfKnowledges;
        }

        public void Update(FieldOfKnowledge item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
