﻿using KnowledgeAccountingSystem.DAL.EF;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class MarkRepository : IRepository<Mark>
    {
        private ApplicationDbContext db;

        public MarkRepository(ApplicationDbContext context)
        {
            this.db = context;
        }
        public void Create(Mark item)
        {
            db.Marks.Add(item);
        }

        public void Delete(string id)
        {
            Mark mark = db.Marks.Include(x => x.ApplicationUser).Include(x => x.MatirialFieldOfKnowledge).FirstOrDefault(x => x.Id == id);
            if (mark != null)
                db.Marks.Remove(mark);
        }

        public Mark Get(string id)
        {
            return db.Marks.FirstOrDefault(m => m.Id == id);
        }

        public IEnumerable<Mark> GetAll()
        {
            return db.Marks.Include(m=>m.ApplicationUser).Include(m => m.MatirialFieldOfKnowledge);
        }

        public void Update(Mark item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public Mark Find(string id)
        {
            return db.Marks.Include(m => m.ApplicationUser).Include(m => m.MatirialFieldOfKnowledge).FirstOrDefault(x => x.Id == id);
        }
    }
}
