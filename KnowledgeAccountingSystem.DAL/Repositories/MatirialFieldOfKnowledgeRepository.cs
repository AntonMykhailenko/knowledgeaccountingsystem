﻿using KnowledgeAccountingSystem.DAL.EF;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class MatirialFieldOfKnowledgeRepository : IRepository<MatirialFieldOfKnowledge>
    {
        private ApplicationDbContext db;

        public MatirialFieldOfKnowledgeRepository(ApplicationDbContext context)
        {
            this.db = context;
        }
        public void Create(MatirialFieldOfKnowledge item)
        {
            db.MatirialFieldOfKnowledges.Add(item);
        }

        public void Delete(string id)
        {
            MatirialFieldOfKnowledge matirialFieldOfKnowledge = db.MatirialFieldOfKnowledges.Find(id);
            if (matirialFieldOfKnowledge != null)
            { 
                db.MatirialFieldOfKnowledges.Remove(matirialFieldOfKnowledge);
            }
        }

        public MatirialFieldOfKnowledge Find(string id)
        {
            return db.MatirialFieldOfKnowledges.Include(m=>m.FieldOfKnowledge).Include(m=>m.Marks).FirstOrDefault(x=>x.Id==id);
        }

        public MatirialFieldOfKnowledge Get(string id)
        {
            return db.MatirialFieldOfKnowledges.Include(m=>m.FieldOfKnowledge).FirstOrDefault(m => m.Id == id);
        }

        public IEnumerable<MatirialFieldOfKnowledge> GetAll()
        {
            return db.MatirialFieldOfKnowledges.Include(x => x.FieldOfKnowledge).Include(m=>m.Marks);
        }

        public void Update(MatirialFieldOfKnowledge item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public WorkWithSubject Pagination(int page = 1)
        {
            int pageSize = 5;
         
            IQueryable<MatirialFieldOfKnowledge> workWithMaterials = db.MatirialFieldOfKnowledges.Include(x => x.FieldOfKnowledge).Include(m => m.Marks).OrderBy(x=>x.FieldOfKnowledge.Name).Skip((page - 1) * pageSize).Take(pageSize);
            IEnumerable<MatirialFieldOfKnowledge> _workWithMaterials = workWithMaterials.ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = GetAll().Count() };
            WorkWithSubject ivm = new WorkWithSubject { PageInfo = pageInfo, WorkWithMaterials = _workWithMaterials };

            return ivm;
        }

    }
}
