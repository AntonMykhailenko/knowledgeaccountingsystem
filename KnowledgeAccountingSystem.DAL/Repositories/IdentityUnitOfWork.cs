﻿using KnowledgeAccountingSystem.DAL.EF;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Identity;
using KnowledgeAccountingSystem.DAL.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class IdentityUnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext db;

        private ApplicationUserManager userManager;
        private FieldOfKnowledgeRepository fieldOfKnowledgeRepository;
        private MatirialFieldOfKnowledgeRepository matirialFieldOfKnowledgeRepository;
        private MarkRepository markRepository;

        public IdentityUnitOfWork()
        {
            db = new ApplicationDbContext();
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            fieldOfKnowledgeRepository = new FieldOfKnowledgeRepository(db);
            matirialFieldOfKnowledgeRepository = new MatirialFieldOfKnowledgeRepository(db);
            markRepository = new MarkRepository(db);
        }

        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

        public FieldOfKnowledgeRepository FieldOfKnowledgeRepositorys
        {
            get { return fieldOfKnowledgeRepository; }
        }
        public MatirialFieldOfKnowledgeRepository MatirialFieldOfKnowledgeRepositorys
        {
            get { return matirialFieldOfKnowledgeRepository; }
        }
        public MarkRepository MarkRepositorys
        {
            get { return markRepository; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}
