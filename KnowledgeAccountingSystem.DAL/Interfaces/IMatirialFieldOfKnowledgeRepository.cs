﻿using KnowledgeAccountingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface IMatirialFieldOfKnowledgeRepository<T> : IRepository<T> where T : class
    {
        WorkWithSubject Pagination();
    }
}
