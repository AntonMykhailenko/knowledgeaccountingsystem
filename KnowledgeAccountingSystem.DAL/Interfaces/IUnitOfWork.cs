﻿using KnowledgeAccountingSystem.DAL.Identity;
using KnowledgeAccountingSystem.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        FieldOfKnowledgeRepository FieldOfKnowledgeRepositorys { get; }
        MatirialFieldOfKnowledgeRepository MatirialFieldOfKnowledgeRepositorys { get; }
        MarkRepository MarkRepositorys { get; }
        Task SaveAsync();
      
    }
}
