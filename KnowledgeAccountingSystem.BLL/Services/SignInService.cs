﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Identity;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class SignInService : ISignInService
    {
        public async Task<Microsoft.AspNet.Identity.Owin.SignInStatus> TwoFactorSignInAsync(string Provider, string Code, bool RememberMe, bool RememberBrowser, ApplicationSignInManager applicationSignInManager)
        {
            return await applicationSignInManager.TwoFactorSignInAsync(Provider, Code, isPersistent: RememberMe, rememberBrowser: RememberBrowser);
        }

        public async Task<string> GetVerifiedUserIdAsync(ApplicationSignInManager applicationSignInManager)
        {
            return await applicationSignInManager.GetVerifiedUserIdAsync();
        }

        public async Task<bool> SendTwoFactorCodeAsync(string selectedProvider, ApplicationSignInManager applicationSignInManager)
        {
            return await applicationSignInManager.SendTwoFactorCodeAsync(selectedProvider);
        }

        public async Task<SignInStatus> ExternalSignInAsync(ExternalLoginInfo loginInfo, ApplicationSignInManager applicationSignInManager)
        {
            return await applicationSignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
        }

        public async Task SignInAsync(ApplicationUserDTO user, ApplicationSignInManager applicationSignInManager)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUserDTO, ApplicationUser>());
            var mapper = new Mapper(config);

            var _user = mapper.Map<ApplicationUserDTO, ApplicationUser>(user);

            await applicationSignInManager.SignInAsync(_user, isPersistent: false, rememberBrowser: false);
        }

    }
}
