﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Repositories;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class AdminService : IAdminService
    {
        IUnitOfWork Database { get; set; }
        IMapper mapper { get; set; }
        public AdminService(IUnitOfWork db)
        {
            Database = db;
            mapper = GetMapper();
        }

        public static MapperConfiguration Configuration()
        {
            var config = new MapperConfiguration(cfg => { cfg.AddProfile<AutomapperProfile>(); });
            return config;
        }

        public static IMapper GetMapper()
        {
            return Configuration().CreateMapper();
        }

        /// <summary>
        /// get all users from certain Field Of Knowledge
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FieldOfKnowledgeDTO> GetAll()
        {       
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<MatirialFieldOfKnowledge, MatirialFieldOfKnowledgeDTO>();
                c.CreateMap<FieldOfKnowledge, FieldOfKnowledgeDTO>();
                
            });
            var mapper = config.CreateMapper();
            */
            return mapper.Map<IEnumerable<FieldOfKnowledge>, IEnumerable<FieldOfKnowledgeDTO>>(Database.FieldOfKnowledgeRepositorys.GetAll());
        }
        /// <summary>
        /// get List Of subject from certain Field Of Knowledge
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<MatirialFieldOfKnowledgeDTO> ListOfMatirialFieldOfKnowledges(string id)
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledge, FieldOfKnowledgeDTO>();
                c.CreateMap<MatirialFieldOfKnowledge, MatirialFieldOfKnowledgeDTO>();                 
            });
            var mapper = config.CreateMapper();
            */
            return mapper.Map<IEnumerable<MatirialFieldOfKnowledge>, IEnumerable<MatirialFieldOfKnowledgeDTO>>(Database.FieldOfKnowledgeRepositorys.GetAllMatirial(id));
        }
        /// <summary>
        /// create subject
        /// </summary>
        /// <param name="matirialFieldOfKnowledgeDTO"></param>
        public void CreateMatirial(MatirialFieldOfKnowledgeDTO matirialFieldOfKnowledgeDTO)
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledgeDTO, FieldOfKnowledge>();
                c.CreateMap<MarkDTO, Mark>();
                c.CreateMap<MatirialFieldOfKnowledgeDTO, MatirialFieldOfKnowledge>();
            });
            var mapper = config.CreateMapper();
            */
            MatirialFieldOfKnowledge item= mapper.Map<MatirialFieldOfKnowledgeDTO, MatirialFieldOfKnowledge>(matirialFieldOfKnowledgeDTO);

            Database.MatirialFieldOfKnowledgeRepositorys.Create(item);
            
            IEnumerable<string> marks = Database.MarkRepositorys.GetAll().Where(x => x.MatirialFieldOfKnowledge.FieldOfKnowledgeId == item.FieldOfKnowledgeId).Select(x=>x.ApplicationUserId).Distinct();

            foreach(var el in marks)
            {
                Mark mark = new Mark();
                mark.MatirialFieldOfKnowledgeId = item.Id;
                mark.Score = 1;
                mark.ApplicationUserId = el;
                Database.MarkRepositorys.Create(mark);
            }

            Database.SaveAsync();
        }
        /// <summary>
        /// Create Field Of Knowledge
        /// </summary>
        /// <param name="fieldOfKnowledgeDTO"></param>
        public void CreateFieldOfKnowledge(FieldOfKnowledgeDTO fieldOfKnowledgeDTO)
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<MatirialFieldOfKnowledgeDTO, MatirialFieldOfKnowledge>();
                c.CreateMap<FieldOfKnowledgeDTO, FieldOfKnowledge>();
            });
            var mapper = config.CreateMapper();
            */
            FieldOfKnowledge item = mapper.Map<FieldOfKnowledgeDTO, FieldOfKnowledge>(fieldOfKnowledgeDTO);
           
            Database.FieldOfKnowledgeRepositorys.Create(item);

            Database.SaveAsync();
        }

        /// <summary>
        /// Find Field Of Knowledge
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FieldOfKnowledgeDTO FindFieldOfKnowledge(string id)
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledge, FieldOfKnowledgeDTO>();
            });
            var mapper = config.CreateMapper();
            */
            return mapper.Map<FieldOfKnowledge, FieldOfKnowledgeDTO>(Database.FieldOfKnowledgeRepositorys.Find(id));
        }
        /// <summary>
        /// Update Field Of Knowledge
        /// </summary>
        /// <param name="fieldOfKnowledgeDTO"></param>
        public void UpdateFieldOfKnowledge(FieldOfKnowledgeDTO fieldOfKnowledgeDTO)
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledgeDTO, FieldOfKnowledge>();
            });
            var mapper = config.CreateMapper();
            */
            FieldOfKnowledge item = mapper.Map<FieldOfKnowledgeDTO, FieldOfKnowledge>(fieldOfKnowledgeDTO);

            Database.FieldOfKnowledgeRepositorys.Update(item);

            Database.SaveAsync();
        }
        /// <summary>
        /// Remove Field Of Knowledge
        /// </summary>
        /// <param name="id"></param>
        public void RemoveFieldOfKnowledge(string id)
        {   
            Database.FieldOfKnowledgeRepositorys.Delete(id);

            Database.SaveAsync();
        }
        /// <summary>
        /// get users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ApplicationUserDTO> GetUser()
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<ApplicationUser, ApplicationUserDTO>();
            });
            var mapper = config.CreateMapper();
            */
            
            return mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<ApplicationUserDTO>>(Database.UserManager.Users.ToList());
        }
        /// <summary>
        /// edit user
        /// </summary>
        /// <param name="applicationUserDTO"></param>
        /// <returns></returns>
        public async Task EditUser(ApplicationUserDTO applicationUserDTO)
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<ApplicationUserDTO, ApplicationUser>();
            });
            var mapper = config.CreateMapper();
            */
            ApplicationUser item1 = mapper.Map<ApplicationUserDTO, ApplicationUser>(applicationUserDTO);

            ApplicationUser item = await Database.UserManager.FindByIdAsync(item1.Id);

            item.Email = item1.Email;

            await Database.UserManager.UpdateAsync(item);

            await Database.SaveAsync();
        }
        /// <summary>
        /// delete user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteUserAsync(string id)
        {

            ApplicationUser applicationUser = await Database.UserManager.FindByIdAsync(id);

            IEnumerable<Mark> marks = Database.MarkRepositorys.GetAll().Where(x => x.ApplicationUserId == applicationUser.Id).ToList();

            foreach(var del in marks)
            {
                Database.MarkRepositorys.Delete(del.Id);
            }

            await Database.UserManager.DeleteAsync(applicationUser);

            await Database.SaveAsync();
        }
        /// <summary>
        /// get user by role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IList<string>> GetUserRoles(string id)
        {
            return await Database.UserManager.GetRolesAsync(id);
        }
        /// <summary>
        /// add new role to user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newRole"></param>
        /// <returns></returns>
        public async Task AddNewRoleAsync(string userId, string newRole)
        {
            ApplicationUser user =await Database.UserManager.FindByIdAsync(userId);

            await Database.UserManager.AddToRoleAsync(userId, newRole);

            await Database.SaveAsync();
        }
        /// <summary>
        /// delete users role
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public async Task DeleteUserRoles(string userId, string role)
        {
            await Database.UserManager.RemoveFromRoleAsync(userId, role);

            await Database.SaveAsync();
        }
        /// <summary>
        /// get all subject
        /// </summary>
        /// <returns></returns>
        public WorkWithSubjectDTO GetAllMaterial(int page = 1)
        {
            /*
            var config = new MapperConfiguration(c =>
            {             
                c.CreateMap<FieldOfKnowledge, FieldOfKnowledgeDTO>();
                c.CreateMap<Mark, MarkDTO>();
                c.CreateMap<MatirialFieldOfKnowledge, MatirialFieldOfKnowledgeDTO>();
                c.CreateMap<PageInfo, PageInfoDTO>();
                c.CreateMap<WorkWithSubject, WorkWithSubjectDTO>();
            });
            var mapper = config.CreateMapper();
            */
            return mapper.Map<WorkWithSubject, WorkWithSubjectDTO>(Database.MatirialFieldOfKnowledgeRepositorys.Pagination(page));
        }
        /// <summary>
        /// delete subject
        /// </summary>
        /// <param name="id"></param>
        public async Task DeleteMaterialAsync(string id)
        {
            IEnumerable<Mark> marks = Database.MarkRepositorys.GetAll().Where(m => m.MatirialFieldOfKnowledgeId == id).ToList();

            foreach (var del in marks)
            {
                Database.MarkRepositorys.Delete(del.Id);
            }

            Database.MatirialFieldOfKnowledgeRepositorys.Delete(id);

            await Database.SaveAsync();
        }
        /// <summary>
        /// find subject
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MatirialFieldOfKnowledgeDTO FindMaterial(string id)
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledge, FieldOfKnowledgeDTO>();
                c.CreateMap<Mark, MarkDTO>();
                c.CreateMap<MatirialFieldOfKnowledge, MatirialFieldOfKnowledgeDTO>();
            });
            var mapper = config.CreateMapper();
            */
            return mapper.Map<MatirialFieldOfKnowledge, MatirialFieldOfKnowledgeDTO>(Database.MatirialFieldOfKnowledgeRepositorys.Find(id));
        }
        /// <summary>
        /// update subject
        /// </summary>
        /// <param name="matirialFieldOfKnowledgeDTO"></param>
        public void UpdateMaterial(MatirialFieldOfKnowledgeDTO matirialFieldOfKnowledgeDTO)
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<MatirialFieldOfKnowledgeDTO, MatirialFieldOfKnowledge>();
            });
            var mapper = config.CreateMapper();
            */
            MatirialFieldOfKnowledge item1 = mapper.Map<MatirialFieldOfKnowledgeDTO, MatirialFieldOfKnowledge>(matirialFieldOfKnowledgeDTO);

            MatirialFieldOfKnowledge item = Database.MatirialFieldOfKnowledgeRepositorys.Find(item1.Id);

            item.Name = item1.Name;

            Database.MatirialFieldOfKnowledgeRepositorys.Update(item);

            Database.SaveAsync();
        }

        /// <summary>
        /// delete field of knowledge and users in this subject
        /// </summary>
        /// <param name="id"></param>
        public async Task DeleteFieldUserMaterialAsync(string id)
        {         
            IEnumerable<MatirialFieldOfKnowledge> matirials = Database.MatirialFieldOfKnowledgeRepositorys.GetAll().Where(m => m.FieldOfKnowledgeId == id).ToList();

            foreach (var delMarks in matirials)
            {
                if(delMarks.Marks.Select(m => m.MatirialFieldOfKnowledgeId).FirstOrDefault()!=null)
                {
                    await DeleteMaterialAsync(delMarks.Marks.Select(m => m.MatirialFieldOfKnowledgeId).FirstOrDefault());
                }

                else
                {
                    Database.MatirialFieldOfKnowledgeRepositorys.Delete(delMarks.Id);
                }
            }

            Database.FieldOfKnowledgeRepositorys.Delete(id);

            await Database.SaveAsync();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
