﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Identity;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class AccountService : IAccountService
    {
        IUnitOfWork Database { get; set; }

        IIdentityMessageService IAccountService.SmsService => Database.UserManager.SmsService;

        public AccountService(IUnitOfWork db)
        {
            Database = db;

            /*
            IKernel ninjectKernel = new StandardKernel();
            ninjectKernel.Bind<IUnitOfWork>().To<IdentityUnitOfWork>();
            Database = ninjectKernel.Get<IUnitOfWork>();
            */
        }
      
        public async Task<Microsoft.AspNet.Identity.IdentityResult> Create(UserDTO userDto, ApplicationSignInManager applicationSignInManager)
        {
            ApplicationSignInManager SignInManager = applicationSignInManager;

            var user = new ApplicationUser { UserName = userDto.Email, Email = userDto.Email };
            Microsoft.AspNet.Identity.IdentityResult result = await Database.UserManager.CreateAsync(user, userDto.Password);

            if (result.Succeeded)
            {
                await Database.UserManager.AddToRoleAsync(user.Id, "user");
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                return result;
            }

            return result;
        }

        public async Task<Microsoft.AspNet.Identity.Owin.SignInStatus> Login(UserDTO userDto, bool rememberMe, ApplicationSignInManager applicationSignInManager) 
        {
            Microsoft.AspNet.Identity.Owin.SignInStatus result = await applicationSignInManager.PasswordSignInAsync(userDto.Email, userDto.Password, rememberMe, shouldLockout: false);
            return result;
        }

        public async Task<bool> VerifyCodeAsync(ApplicationSignInManager applicationSignInManager)
        {
            return await applicationSignInManager.HasBeenVerifiedAsync();
        }

        public async Task<IdentityResult> ConfirmEmail(string userId, string code)
        {
            return await Database.UserManager.ConfirmEmailAsync(userId, code);
        }

        public async Task<ApplicationUserDTO> FindByName(string email) 
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, ApplicationUserDTO>());
            var mapper = new Mapper(config);

            var result= await Database.UserManager.FindByNameAsync(email);

            return mapper.Map<ApplicationUser, ApplicationUserDTO>(result);
        }
        
        public async Task<bool> IsEmailConfirmed(string id) 
        {
            return await Database.UserManager.IsEmailConfirmedAsync(id);
        }

        public async Task<IdentityResult> ResetPassword(string id, string code, string password)
        {
            return await Database.UserManager.ResetPasswordAsync(id, code, password);
        }

        public async Task<IList<string>> GetValidTwoFactorProviders(string userId)
        {
            return await Database.UserManager.GetValidTwoFactorProvidersAsync(userId);
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUserDTO user)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUserDTO, ApplicationUser>());
            var mapper = new Mapper(config);

            var _user = mapper.Map<ApplicationUserDTO, ApplicationUser>(user);

            return await Database.UserManager.CreateAsync(_user);
        }

        public async Task<IdentityResult> AddLoginAsync(ApplicationUserDTO user, ExternalLoginInfo info)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUserDTO, ApplicationUser>());
            var mapper = new Mapper(config);

            var _user = mapper.Map<ApplicationUserDTO, ApplicationUser>(user);

            return await Database.UserManager.AddLoginAsync(_user.Id, info.Login);
        }

        public async Task<string> GetPhoneNumberAsync(string userId)
        {
            return await Database.UserManager.GetPhoneNumberAsync(userId);
        }

        public async Task<bool> GetTwoFactorEnabledAsync(string userId)
        {
            return await Database.UserManager.GetTwoFactorEnabledAsync(userId);
        }
        public async Task<IList<UserLoginInfo>> GetLoginsAsync(string userId)
        {
            return await Database.UserManager.GetLoginsAsync(userId);
        }

        public async Task<IdentityResult> RemoveLoginAsync(string userId, string loginProvider, string providerKey)
        {
            return await Database.UserManager.RemoveLoginAsync(userId, new UserLoginInfo(loginProvider, providerKey));
        }

        public async Task<ApplicationUserDTO> FindByIdAsync(string userId)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, ApplicationUserDTO>());
            var mapper = new Mapper(config);

            var result = await Database.UserManager.FindByIdAsync(userId);

            return mapper.Map<ApplicationUser, ApplicationUserDTO>(result);
        }

        public async Task<string> GenerateChangePhoneNumberTokenAsync(string userId, string number)
        {           
            return await Database.UserManager.GenerateChangePhoneNumberTokenAsync(userId, number);
        }

        public async Task<IdentityResult> SetTwoFactorEnabledAsync(string userId, bool isTrueOrFalse)
        {
            return await Database.UserManager.SetTwoFactorEnabledAsync(userId, isTrueOrFalse);
        }

        public async Task<IdentityResult> ChangePhoneNumberAsync(string userId, string phoneNumber, string code)
        {
            return await Database.UserManager.ChangePhoneNumberAsync(userId, phoneNumber, code);
        }

        public async Task<IdentityResult> SetPhoneNumberAsync(string userId, string secPar)
        {
            return await Database.UserManager.SetPhoneNumberAsync(userId, secPar);
        }

        public async Task<IdentityResult> ChangePasswordAsync(string userId, string oldPassword, string newPassword)
        {
            return await Database.UserManager.ChangePasswordAsync(userId, oldPassword, newPassword);
        }

        public async Task<IdentityResult> AddPasswordAsync(string userId, string newPassword)
        {
            return await Database.UserManager.AddPasswordAsync(userId, newPassword);
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, ExternalLoginInfo loginInfo)
        {
            return await Database.UserManager.AddLoginAsync(userId, loginInfo.Login);
        }

        public ApplicationUserDTO FindById(string userId)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, ApplicationUserDTO>());
            var mapper = new Mapper(config);

            var result = Database.UserManager.FindById(userId);

            return mapper.Map<ApplicationUser, ApplicationUserDTO>(result);           
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
