﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Infrastructure;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Repositories;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class ManagerService : IManagerService
    {
        IUnitOfWork Database { get; set; }
        IMapper mapper { get; set; }
        public ManagerService(IUnitOfWork db)
        {
            Database = db;
            mapper = GetMapper();
        }

        public static MapperConfiguration Configuration()
        {
            var config = new MapperConfiguration(cfg => { cfg.AddProfile<AutomapperProfile>(); });
            return config;
        }

        public static IMapper GetMapper()
        {
            return Configuration().CreateMapper();
        }

        /// <summary>
        /// get all users which satisfy some condition
        /// </summary>
        /// <param name="email"></param>
        /// <param name="matirial"></param>
        /// <param name="mark"></param>
        /// <returns></returns>
        public async Task<List<ApplicationUserDTO>> GetAllUsersWhichEqualsSomeParamsAsync(string email, IEnumerable<string> matirial, IEnumerable<int> mark)
        {
            /*
            string phone = null;
            if (phone == null)
                throw new ValidationException("Телефон не найден", "");
            
            /////////////////////////

            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<ApplicationUser, ApplicationUserDTO>();
            });
            var mapper = config.CreateMapper();
            */

            List<IEnumerable< Mark> > matirials = GetMatirial(matirial);                

            List<ApplicationUser> users = new List<ApplicationUser>();

            int[] _mark = mark.ToArray();

            string[] temporaryUser = matirials[0].Where(m => m.Score >= _mark[0]).Select(x => x.ApplicationUserId).ToArray();

            List<IEnumerable<Mark>> _mat = GetMatirialWithSomeUser(matirials, _mark);

            for (int i=0; i<temporaryUser.Length; i++)
            {
                int j = 0;
                foreach(var els in _mat)
                {
                    foreach(var el in els)
                    {
                        if(el.ApplicationUserId==temporaryUser[i])
                        {
                            j++;
                        }
                    }
                    if(j== matirial.ToArray().Length)
                    {
                        users.Add(await Database.UserManager.FindByIdAsync(temporaryUser[i]));
                    }
                }
            }

            return mapper.Map<List<ApplicationUser>, List<ApplicationUserDTO>>(users);

        }

        public List<IEnumerable<Mark>> GetMatirial(IEnumerable<string> matirial)
        {
            IEnumerable<Mark> allMakrs = Database.MarkRepositorys.GetAll();

            List<IEnumerable<Mark>> matirials = new List<IEnumerable<Mark>>();

            string[] _matirial = matirial.ToArray();

            for (int i = 0; i < _matirial.Length; i++)
            {
                matirials.Add(allMakrs.Where(m => m.MatirialFieldOfKnowledgeId == _matirial[i]).ToList());
            }
            return matirials;
        }

        public List<IEnumerable<Mark>> GetMatirialWithSomeUser(List<IEnumerable<Mark>> matirials, int[] _mark)
        {
            List<IEnumerable<Mark>> _mat = new List<IEnumerable<Mark>>();

            for (int i = 0; i < _mark.Length; i++)
            {
                _mat.Add(matirials[i].Where(m => m.Score >= _mark[i]).ToList());
            }
            return _mat;
        }


        /// <summary>
        /// get user for report
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<MarkDTO>> GetUser(string id)
        {
            /*
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<ApplicationUser, ApplicationUserDTO>();
                c.CreateMap<MatirialFieldOfKnowledge, MatirialFieldOfKnowledgeDTO>();
                c.CreateMap<Mark, MarkDTO>();                
            });
            var mapper = config.CreateMapper();
            */

            ApplicationUser user =await Database.UserManager.FindByIdAsync(id);

            IEnumerable<Mark> marks = Database.MarkRepositorys.GetAll().Where(m => m.ApplicationUserId == user.Id).ToList();

            IEnumerable<MarkDTO> _marks = mapper.Map<IEnumerable<Mark>, IEnumerable<MarkDTO>>(Database.MarkRepositorys.GetAll().Where(m => m.ApplicationUserId == user.Id).ToList());          

            return mapper.Map<IEnumerable<Mark>, IEnumerable<MarkDTO>>(Database.MarkRepositorys.GetAll().Where(m => m.ApplicationUserId == user.Id).ToList());
        }

    }
}
