﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Repositories;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }
        IMapper mapper { get; set; }
        public UserService(IUnitOfWork db)
        {
            Database = db;
            mapper = GetMapper();
        }

        public static MapperConfiguration Configuration()
        {
            var config = new MapperConfiguration(cfg => { cfg.AddProfile<AutomapperProfile>(); });
            return config;
        }

        public static IMapper GetMapper()
        {
            return Configuration().CreateMapper();
        }

        /// <summary>
        /// get all marks
        /// </summary>
        /// <returns></returns>

        public IEnumerable<MarkDTO> GetAll()
        {
           
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<MatirialFieldOfKnowledge, MatirialFieldOfKnowledgeDTO>();
                c.CreateMap<ApplicationUser, ApplicationUserDTO>();
                c.CreateMap<Mark, MarkDTO>();

            });
            var mapper = config.CreateMapper();

            return mapper.Map<IEnumerable<Mark>, IEnumerable<MarkDTO>>(Database.MarkRepositorys.GetAll());
        }
        /// <summary>
        /// List Of Matirial Field Of Knowledges
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<MatirialFieldOfKnowledgeDTO> ListOfMatirialFieldOfKnowledges(string id)
        {
           
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledge, FieldOfKnowledgeDTO>();
                c.CreateMap<MatirialFieldOfKnowledge, MatirialFieldOfKnowledgeDTO>();
            });
            var mapper = config.CreateMapper();

            return mapper.Map<IEnumerable<MatirialFieldOfKnowledge>, IEnumerable<MatirialFieldOfKnowledgeDTO>>(Database.FieldOfKnowledgeRepositorys.GetAllMatirial(id));
        }
        /// <summary>
        /// write in db Answer
        /// </summary>
        /// <param name="email"></param>
        /// <param name="matirial"></param>
        /// <param name="mark"></param>
        /// <returns></returns>
        public async Task CreateUserAnswerAsync(string email, IEnumerable<string> matirial, IEnumerable<int> mark)
        {
            ApplicationUser user =await Database.UserManager.FindByNameAsync(email);
           

            string [] _matirial = matirial.ToArray();
            int [] _mark = mark.ToArray();

            for (int i=0; i<_matirial.Length; i++)
            {
                Mark marks = new Mark();
                marks.MatirialFieldOfKnowledgeId = _matirial[i];
                marks.Score = _mark[i];
                marks.ApplicationUserId = user.Id;
                Database.MarkRepositorys.Create(marks);
            }
            await Database.SaveAsync();

        }
        /// <summary>
        /// answered user or not
        /// </summary>
        /// <param name="email"></param>
        /// <param name="matirial"></param>
        /// <returns></returns>
        public bool IsUserMaterial(string email, IEnumerable<string> matirial)
        {
            IEnumerable<Mark> allMakrs = Database.MarkRepositorys.GetAll();

            List<IEnumerable<Mark>> matirials = new List<IEnumerable<Mark>>();

            string[] _matirial = matirial.ToArray();

            for (int i = 0; i < _matirial.Length; i++)
            {
                matirials.Add(allMakrs.Where(m => m.MatirialFieldOfKnowledgeId == _matirial[i]).ToList());
            }

            var user = matirials.Select(x => x.Where(m => m.ApplicationUser.Email == email).ToList());

            var userList = user.Select(m => m.Select(u => u.ApplicationUser.Email)).ToArray();

            if(userList[0].Count() != 0)
            {
                return true;               
            }

            return false;
        }
        /// <summary>
        /// update answer
        /// </summary>
        /// <param name="email"></param>
        /// <param name="matirial"></param>
        /// <param name="mark"></param>
        /// <returns></returns>
        public async Task UpdateMatirialMarksAsync(string email, IEnumerable<string> matirial, IEnumerable<int> mark)
        {
            ApplicationUser user = await Database.UserManager.FindByNameAsync(email);

            Mark markDb;
            int[] _mark = mark.ToArray();
            int i = 0;

            foreach (var el in matirial)
            {
                markDb= Database.MarkRepositorys.GetAll().Where(m => m.ApplicationUserId == user.Id && m.MatirialFieldOfKnowledgeId == el).First();

                Mark updateMarkDb = Database.MarkRepositorys.Get(markDb.Id);

                updateMarkDb.Score = _mark[i];

                Database.MarkRepositorys.Update(updateMarkDb);

                i++;
            }

            await Database.SaveAsync();
        }
        /// <summary>
        /// is subject in db or not
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsMaterial(string id)
        {
            FieldOfKnowledge matirial = Database.FieldOfKnowledgeRepositorys.Find(id);
            if(matirial==null)
            {
                return true;
            }

            return false;
        }

    }
}
