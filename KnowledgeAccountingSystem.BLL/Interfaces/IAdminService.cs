﻿using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IAdminService
    {
        IEnumerable<FieldOfKnowledgeDTO> GetAll();
        IEnumerable<MatirialFieldOfKnowledgeDTO> ListOfMatirialFieldOfKnowledges(string id);
        void CreateMatirial(MatirialFieldOfKnowledgeDTO matirialFieldOfKnowledgeDTO);
        void CreateFieldOfKnowledge(FieldOfKnowledgeDTO fieldOfKnowledgeDTO);
        FieldOfKnowledgeDTO FindFieldOfKnowledge(string id);
        void UpdateFieldOfKnowledge(FieldOfKnowledgeDTO fieldOfKnowledgeDTO);
        void RemoveFieldOfKnowledge(string id);
        IEnumerable<ApplicationUserDTO> GetUser();
        Task EditUser(ApplicationUserDTO applicationUserDTO);
        Task DeleteUserAsync(string id);
        Task<IList<string>> GetUserRoles(string id);
        Task AddNewRoleAsync(string email, string newRole);
        Task DeleteUserRoles(string userId, string role);
        WorkWithSubjectDTO GetAllMaterial(int page = 1);
        MatirialFieldOfKnowledgeDTO FindMaterial(string id);
        Task DeleteMaterialAsync(string id);
        void UpdateMaterial(MatirialFieldOfKnowledgeDTO matirialFieldOfKnowledgeDTO);
        Task DeleteFieldUserMaterialAsync(string id);
        void Dispose();
    }
}
