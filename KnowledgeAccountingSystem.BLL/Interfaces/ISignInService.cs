﻿using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface ISignInService 
    {
        Task<Microsoft.AspNet.Identity.Owin.SignInStatus> TwoFactorSignInAsync(string Provider, string Code, bool RememberMe, bool RememberBrowser, ApplicationSignInManager applicationSignInManager);
        Task<string> GetVerifiedUserIdAsync(ApplicationSignInManager applicationSignInManager);
        Task<bool> SendTwoFactorCodeAsync(string selectedProvider, ApplicationSignInManager applicationSignInManager);
        Task<SignInStatus> ExternalSignInAsync(ExternalLoginInfo loginInfo, ApplicationSignInManager applicationSignInManager);
        Task SignInAsync(ApplicationUserDTO user, ApplicationSignInManager applicationSignInManager);
    }
}
