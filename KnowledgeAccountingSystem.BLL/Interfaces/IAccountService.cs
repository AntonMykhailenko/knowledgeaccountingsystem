﻿using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Identity;
using KnowledgeAccountingSystem.DAL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IAccountService : IDisposable
    {
        Task<Microsoft.AspNet.Identity.IdentityResult> Create(UserDTO userDto, ApplicationSignInManager applicationSignInManager);
        Task<Microsoft.AspNet.Identity.Owin.SignInStatus> Login(UserDTO userDto, bool rememberMe, ApplicationSignInManager applicationSignInManager);
        Task<bool> VerifyCodeAsync(ApplicationSignInManager applicationSignInManager);
        Task<IdentityResult> ConfirmEmail(string userId, string code);
        Task<ApplicationUserDTO> FindByName(string email);
        Task<bool> IsEmailConfirmed(string id);
        Task<IdentityResult> ResetPassword(string id, string code, string password);
        Task<IList<string>> GetValidTwoFactorProviders(string userId);
        Task<IdentityResult> CreateAsync(ApplicationUserDTO user);
        Task<IdentityResult> AddLoginAsync(ApplicationUserDTO user, ExternalLoginInfo info);
        Task<string> GetPhoneNumberAsync(string userId);
        Task<bool> GetTwoFactorEnabledAsync(string userId);
        Task<IList<UserLoginInfo>> GetLoginsAsync(string userId);
        Task<IdentityResult> RemoveLoginAsync(string userId, string loginProvider, string providerKey);
        Task<ApplicationUserDTO> FindByIdAsync(string userId);
        Task<string> GenerateChangePhoneNumberTokenAsync(string userId, string number);
        IIdentityMessageService SmsService { get; }
        Task<IdentityResult> SetTwoFactorEnabledAsync(string userId, bool isTrueOrFalse);
        Task<IdentityResult> ChangePhoneNumberAsync(string userId, string phoneNumber, string code);
        Task<IdentityResult> SetPhoneNumberAsync(string userId, string secPar);
        Task<IdentityResult> ChangePasswordAsync(string userId, string oldPassword, string newPassword);
        Task<IdentityResult> AddPasswordAsync(string userId, string newPassword);
        Task<IdentityResult> AddLoginAsync(string userId, ExternalLoginInfo loginInfo);
        ApplicationUserDTO FindById(string userId);
    }
}
