﻿using KnowledgeAccountingSystem.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IManagerService
    {
        Task<List<ApplicationUserDTO>> GetAllUsersWhichEqualsSomeParamsAsync(string email, IEnumerable<string> matirial, IEnumerable<int> mark);
        Task<IEnumerable<MarkDTO>> GetUser(string id);
    }
}
