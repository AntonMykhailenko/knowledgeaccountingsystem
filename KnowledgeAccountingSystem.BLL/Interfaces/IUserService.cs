﻿using KnowledgeAccountingSystem.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IUserService
    {
        IEnumerable<MarkDTO> GetAll();
        IEnumerable<MatirialFieldOfKnowledgeDTO> ListOfMatirialFieldOfKnowledges(string id);
        Task CreateUserAnswerAsync(string email, IEnumerable<string> matirial, IEnumerable<int> mark);
        bool IsUserMaterial(string email, IEnumerable<string> matirial);
        bool IsMaterial(string id);
        Task UpdateMatirialMarksAsync(string email, IEnumerable<string> matirial, IEnumerable<int> mark);
    }
}
