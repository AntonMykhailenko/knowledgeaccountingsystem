﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<MatirialFieldOfKnowledge, MatirialFieldOfKnowledgeDTO>();
            CreateMap<FieldOfKnowledge, FieldOfKnowledgeDTO>();
            CreateMap<Mark, MarkDTO>();
            CreateMap<FieldOfKnowledgeDTO, FieldOfKnowledge>();
            CreateMap<MarkDTO, Mark>();
            CreateMap<MatirialFieldOfKnowledgeDTO, MatirialFieldOfKnowledge>();
            CreateMap<ApplicationUser, ApplicationUserDTO>();
            CreateMap<ApplicationUserDTO, ApplicationUser>();
            CreateMap<PageInfo, PageInfoDTO>();
            CreateMap<WorkWithSubject, WorkWithSubjectDTO>();
        }
    }
}
