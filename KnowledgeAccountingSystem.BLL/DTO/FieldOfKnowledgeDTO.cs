﻿using KnowledgeAccountingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.DTO
{
    public class FieldOfKnowledgeDTO
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public virtual ICollection<ApplicationUserDTO> ApplicationUsers { get; set; }
        public ICollection<MatirialFieldOfKnowledgeDTO> MatirialFieldOfKnowledges { get; set; }

        public FieldOfKnowledgeDTO()
        {
            ApplicationUsers = new List<ApplicationUserDTO>();
            MatirialFieldOfKnowledges = new List<MatirialFieldOfKnowledgeDTO>();
        }
    }
}
