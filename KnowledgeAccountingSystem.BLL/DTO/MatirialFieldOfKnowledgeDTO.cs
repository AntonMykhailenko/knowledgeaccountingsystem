﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.DTO
{
    public class MatirialFieldOfKnowledgeDTO
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string FieldOfKnowledgeId { get; set; } = Guid.NewGuid().ToString();
        public FieldOfKnowledgeDTO FieldOfKnowledge { get; set; }
        public ICollection<MarkDTO> Marks { get; set; }
        public MatirialFieldOfKnowledgeDTO()
        {
            Marks = new List<MarkDTO>();
        }
    }
}
