﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.DTO
{
    public class MarkDTO
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public int Score { get; set; }
        public string MatirialFieldOfKnowledgeId { get; set; } = Guid.NewGuid().ToString();
        public MatirialFieldOfKnowledgeDTO MatirialFieldOfKnowledge { get; set; }
        public string ApplicationUserId { get; set; } = Guid.NewGuid().ToString();
        public ApplicationUserDTO ApplicationUser { get; set; }

    }
}
