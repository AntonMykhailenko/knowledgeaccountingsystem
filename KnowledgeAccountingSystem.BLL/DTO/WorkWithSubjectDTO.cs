﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.DTO
{
    public class WorkWithSubjectDTO
    {
        public IEnumerable<MatirialFieldOfKnowledgeDTO> WorkWithMaterials { get; set; }
        public PageInfoDTO PageInfo { get; set; }
    }
}
