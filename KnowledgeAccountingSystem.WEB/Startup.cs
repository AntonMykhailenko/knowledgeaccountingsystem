﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KnowledgeAccountingSystem.WEB.Startup))]
namespace KnowledgeAccountingSystem.WEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
