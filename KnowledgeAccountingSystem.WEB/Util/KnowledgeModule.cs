﻿using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeAccountingSystem.WEB.Util
{
    public class KnowledgeModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAccountService>().To<AccountService>();
            Bind<ISignInService>().To<SignInService>();
            Bind<IAdminService>().To<AdminService>();
            Bind<IUserService>().To<UserService>();
            Bind<IManagerService>().To<ManagerService>();
        }
    }
}