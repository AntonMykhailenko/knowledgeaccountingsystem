﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Infrastructure;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Services;
using KnowledgeAccountingSystem.WEB.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace KnowledgeAccountingSystem.WEB.Controllers
{
    [Authorize(Roles = "admin, manager")]
    public class ManagerController : Controller
    {
        private IManagerService managerService;
        private IUserService userService;
        private IAdminService adminService;
        private IAccountService accountService;

        /// <summary>
        /// dependency injection
        /// </summary>
        /// <param name="managerServ"></param>
        /// <param name="userServ"></param>
        /// <param name="adminServ"></param>
        /// <param name="accountServ"></param>
        public ManagerController(IManagerService managerServ,IUserService userServ,IAdminService adminServ,IAccountService accountServ)
        {
            managerService = managerServ;
            userService = userServ;
            adminService = adminServ;
            accountService = accountServ;
        }

        /// <summary>
        /// get all fields of knowledge
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledgeDTO, IndexViewModels>();

            });
            var mapper = config.CreateMapper();

            return View(mapper.Map<IEnumerable<FieldOfKnowledgeDTO>, IEnumerable<IndexViewModels>>(adminService.GetAll().ToList()));
        }
        /// <summary>
        /// List of subjects field of knowledges
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ListOfMatirialFieldOfKnowledges(string id)
        {
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Manager", new { mistakeName = mistakeName });
            }

            if (userService.IsMaterial(id))
            {
                string mistakeName = "Material with this id is empty or absent";
                return RedirectToAction("Mistakes", "Manager", new { mistakeName = mistakeName });
            }

            //var listOfMatirial = userService.ListOfMatirialFieldOfKnowledges(id);

            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<MatirialFieldOfKnowledgeDTO, ListOfMatirialFieldOfKnowledgesViewModels>();
            });
            var mapper = config.CreateMapper();

            var listOfMatirial = mapper.Map<IEnumerable<MatirialFieldOfKnowledgeDTO>, IEnumerable<ListOfMatirialFieldOfKnowledgesViewModels>>(userService.ListOfMatirialFieldOfKnowledges(id));


            if (listOfMatirial == null)
            {
                return HttpNotFound();
            }

            return View(listOfMatirial);
        }
        /// <summary>
        /// get all users which satisfy condition (marks)
        /// </summary>
        /// <param name="matirial"></param>
        /// <param name="mark"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Result(IEnumerable<string> matirial, IEnumerable<int> mark)
        {           

            string email = HttpContext.User.Identity.Name;
            
            if (email == null || matirial == null || mark == null)
            {
                string mistakeName = "Material with this id is empty or absent";
                return RedirectToAction("Mistakes", "Manager", new { mistakeName = mistakeName });
            }

            foreach (var index in mark)
            {
                if (index < 1 || index > 5)
                {
                    string mistakeName = "Dont try do it";
                    return RedirectToAction("Mistakes", "Manager", new { mistakeName = mistakeName });
                }
            }

            /*
            try
            {
                var order = await managerService.GetAllUsersWhichEqualsSomeParamsAsync(email, matirial, mark);

                return View(order);
            }
            catch (ValidationException ex)
            {
                return Content(ex.Message);
            }
            */

            if (ModelState.IsValid)
            {
                return View(await managerService.GetAllUsersWhichEqualsSomeParamsAsync(email, matirial, mark));
            }

            string mistake = "You try to made a mistake";
            return RedirectToAction("Mistakes", "Manager", new { mistakeName = mistake });
        }
        /// <summary>
        /// Report about user which you have chosen
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Report(string id)
        {
            List<ReportViewModels> reportViewModels = new List<ReportViewModels>(); 

            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Manager", new { mistakeName = mistakeName });
            }

            IEnumerable<MarkDTO> applicationUser = await managerService.GetUser(id);

            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            foreach(var el in applicationUser)
            {
                reportViewModels.Add(new ReportViewModels 
                { 
                    User=el.ApplicationUser.Email,
                    FieldName= adminService.FindFieldOfKnowledge(el.MatirialFieldOfKnowledge.FieldOfKnowledgeId).Name,
                    MatirialName= el.MatirialFieldOfKnowledge.Name,
                    Score= el.Score
                });
            }


            IEnumerable<ReportViewModels> sortedUsers = reportViewModels.OrderBy(m => m.FieldName).ToList();

            return View(sortedUsers);
        }
        /// <summary>
        /// when something happens incorrect
        /// </summary>
        /// <param name="mistakeName"></param>
        /// <returns></returns>
        public ActionResult Mistakes(string mistakeName)
        {
            ViewBag.Messege = mistakeName;
            return View();
        }


    }
}