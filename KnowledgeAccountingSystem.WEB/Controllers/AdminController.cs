﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Services;

using KnowledgeAccountingSystem.WEB.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace KnowledgeAccountingSystem.WEB.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private IAdminService adminService;
        private IAccountService accountService;
        private IUserService userService;

        /// <summary>
        /// dependency injection
        /// </summary>
        /// <param name="adminServ"></param>
        /// <param name="accountServ"></param>
        /// <param name="userServ"></param>
        public AdminController(IAdminService adminServ, IAccountService accountServ,IUserService userServ)
        {
            adminService = adminServ;
            accountService = accountServ;
            userService = userServ;
        }
        /// <summary>
        /// get all fields of knowledge
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledgeDTO, IndexViewModels>();

            });
            var mapper = config.CreateMapper();

            return View(mapper.Map<IEnumerable<FieldOfKnowledgeDTO>, IEnumerable<IndexViewModels>>(adminService.GetAll().ToList()));
        }
        /// <summary>
        /// List of subjects field of knowledges
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ListOfMatirialFieldOfKnowledges(string id)
        {
      
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            if (userService.IsMaterial(id))
            {
                string mistakeName = "Material with this id is empty or absent";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<MatirialFieldOfKnowledgeDTO, ListOfMatirialFieldOfKnowledgesViewModels>();
            });
            var mapper = config.CreateMapper();

            return View(mapper.Map<IEnumerable<MatirialFieldOfKnowledgeDTO>, IEnumerable<ListOfMatirialFieldOfKnowledgesViewModels>>(adminService.ListOfMatirialFieldOfKnowledges(id)));

        }
        /// <summary>
        /// View create subject
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateMatirial()
        {
            ViewBag.FieldOfKnowledgeId = new SelectList(adminService.GetAll(), "Id", "Name");
            return View();
        }

        /// <summary>
        /// Sending data to server with parameter which will create subject
        /// </summary>
        /// <param name="matirialFieldOfKnowledge"></param>
        /// <returns></returns>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateMatirial(CreateMatirialViewModels matirialFieldOfKnowledgeView)
        {
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<CreateMatirialViewModels, MatirialFieldOfKnowledgeDTO>();
            });
            var mapper = config.CreateMapper();

            MatirialFieldOfKnowledgeDTO matirialFieldOfKnowledge = mapper.Map<CreateMatirialViewModels, MatirialFieldOfKnowledgeDTO>(matirialFieldOfKnowledgeView);

            if (ModelState.IsValid)
            {
                adminService.CreateMatirial(matirialFieldOfKnowledge);
                return RedirectToAction("Index");
            }

            ViewBag.FieldOfKnowledgeId = new SelectList(adminService.GetAll(), "Id", "Name", matirialFieldOfKnowledge.FieldOfKnowledgeId);
            return View(matirialFieldOfKnowledge);
        }

        /// <summary>
        /// View create field of knowledge
        /// </summary>
        /// <returns></returns>

        public ActionResult Create()
        {
            ViewBag.FieldOfKnowledgeId = new SelectList(adminService.GetAll(), "Id", "Name");
            return View();
        }

        /// <summary>
        /// Sending data to server with parameter which will create field of knowledge
        /// </summary>
        /// <param name="fieldOfKnowledge"></param>
        /// <returns></returns>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateViewModels fieldOfKnowledgeView)
        {
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<CreateViewModels, FieldOfKnowledgeDTO>();
            });
            var mapper = config.CreateMapper();

            FieldOfKnowledgeDTO fieldOfKnowledge = mapper.Map<CreateViewModels, FieldOfKnowledgeDTO>(fieldOfKnowledgeView);

            if (ModelState.IsValid)
            {
                adminService.CreateFieldOfKnowledge(fieldOfKnowledge);

                return RedirectToAction("Index");
            }

            return View(fieldOfKnowledge);
        }

        /// <summary>
        /// View edit field of knowledge
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }
            
             var config = new MapperConfiguration(c =>
             {
                c.CreateMap<FieldOfKnowledgeDTO, EditViewModels>();
             });
             var mapper = config.CreateMapper();

             EditViewModels fieldOfKnowledge = mapper.Map<FieldOfKnowledgeDTO, EditViewModels>(adminService.FindFieldOfKnowledge(id));           

            if (fieldOfKnowledge == null)
            {
                return HttpNotFound();
            }
            return View(fieldOfKnowledge);
        }

        /// <summary>
        /// Sending data to server with parameter which will edit field of knowledge
        /// </summary>
        /// <param name="fieldOfKnowledge"></param>
        /// <returns></returns>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditViewModels fieldOfKnowledgeView)
        {
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<EditViewModels, FieldOfKnowledgeDTO>();
            });
            var mapper = config.CreateMapper();

            FieldOfKnowledgeDTO fieldOfKnowledge = mapper.Map<EditViewModels, FieldOfKnowledgeDTO>(fieldOfKnowledgeView);

            if (ModelState.IsValid)
            {
                adminService.UpdateFieldOfKnowledge(fieldOfKnowledge);
                return RedirectToAction("Index");
            }
            return View(fieldOfKnowledge);
        }

        /// <summary>
        /// View delete field of knowledge
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledgeDTO, DeleteViewModels>();
            });
            var mapper = config.CreateMapper();

            DeleteViewModels fieldOfKnowledge = mapper.Map<FieldOfKnowledgeDTO, DeleteViewModels>(adminService.FindFieldOfKnowledge(id));

            if (fieldOfKnowledge == null)
            {
                return HttpNotFound();
            }
            return View(fieldOfKnowledge);
        }

        /// <summary>
        /// Sending data to server with parameter which will delete field of knowledge
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedAsync(string id)
        {
            await adminService.DeleteFieldUserMaterialAsync(id);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// get all users
        /// </summary>
        /// <returns></returns>

        public ActionResult GetUser()
        {
            return View(adminService.GetUser());
        }
        /// <summary>
        /// View edit user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditUser(string id)
        {
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }
            
            ApplicationUserDTO applicationUser = await accountService.FindByIdAsync(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        /// <summary>
        /// Sending data to server with parameter which will edit user
        /// </summary>
        /// <param name="applicationUser"></param>
        /// <returns></returns>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditUser([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUserDTO applicationUser)
        {
            if (ModelState.IsValid)
            {
                await adminService.EditUser(applicationUser);
  
                return RedirectToAction("Index");
            }
            return View(applicationUser);
        }

        /// <summary>
        /// View delete user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public async Task<ActionResult> DeleteUser(string id)
        {
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            ApplicationUserDTO applicationUser = await accountService.FindByIdAsync(id);

            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        /// <summary>
        /// Sending data to server with parameter which will delete user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteUserConfirmed(string id)
        {
            await adminService.DeleteUserAsync(id);
        
            return RedirectToAction("Index");
        }
        /// <summary>
        /// View add new role to user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> AddNewRole(string id)
        {
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }
            UserRolesViewModels userRoles = new UserRolesViewModels();

            userRoles.User = await accountService.FindByIdAsync(id);

            if (userRoles.User==null)
            {
                string mistakeName = "User with this id absent";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            userRoles.Roles = await adminService.GetUserRoles(id);

            if (userRoles.User == null)
            {
                return HttpNotFound();
            }

            return View(userRoles);
        }

        /// <summary>
        /// Sending data to server with parameter which will add new role to user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userRoles"></param>
        /// <returns></returns>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddNewRole(string id, UserRolesViewModels userRoles)
        {
            string userId = id;

            userRoles.Roles = await adminService.GetUserRoles(userId);

            foreach (var el in userRoles.Roles)
            {
                if(el==userRoles.NewRoles)
                {
                    return RedirectToAction("Index");
                }
            }

            if (ModelState.IsValid)
            {
                await adminService.AddNewRoleAsync(userId, userRoles.NewRoles);
                return RedirectToAction("Index");
            }
            return View(userRoles);
        }

        /// <summary>
        /// View delete role to user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public async Task<ActionResult> DeleteUserRoles(string id)
        {
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            UserRolesViewModels userRoles = new UserRolesViewModels();

            userRoles.User = await accountService.FindByIdAsync(id);

            if (userRoles.User == null)
            {
                string mistakeName = "User with this id absent";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            userRoles.Roles = await adminService.GetUserRoles(id);

            if (userRoles.User == null)
            {
                return HttpNotFound();
            }

            return View(userRoles);
        }

        /// <summary>
        /// Sending data to server with parameter which will delete role to user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deleteRoles"></param>
        /// <returns></returns>

        [HttpPost, ActionName("DeleteUserRoles")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteUserRolesConfirmed(string id, string deleteRoles)
        {
            string userId = id;

            if(deleteRoles==null)
            {
                string mistakeName = "User does not have role";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            if(userId!=null && deleteRoles!= null)
            { 
                await adminService.DeleteUserRoles(id, deleteRoles);
                return RedirectToAction("Index");         
            }

            return View();            
        }
        /// <summary>
        /// when something happens incorrect
        /// </summary>
        /// <param name="mistakeName"></param>
        /// <returns></returns>
        public ActionResult Mistakes(string mistakeName)
        {
            ViewBag.Messege = mistakeName;
            return View();
        }
        /// <summary>
        /// get all material that you could work with them (edit, delete)
        /// </summary>
        /// <returns></returns>
        public ActionResult WorkWithMaterial(int page = 1)
        {
            var config = new MapperConfiguration(c =>
            {            
                c.CreateMap<WorkWithSubjectDTO, WorkWithMaterialViewModel>();
            });
            var mapper = config.CreateMapper();
            
            if(page<1)
            {
                string mistakeName = "Dont do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            WorkWithMaterialViewModel result = mapper.Map<WorkWithSubjectDTO, WorkWithMaterialViewModel>(adminService.GetAllMaterial(page));

            if(result.WorkWithMaterials.Count()==0)
            {
                page = 1;
                result = mapper.Map<WorkWithSubjectDTO, WorkWithMaterialViewModel>(adminService.GetAllMaterial(page));
            }

            return View(result);

        }
        /// <summary>
        /// View delete subject
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteMaterial(string id)
        {
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<MatirialFieldOfKnowledgeDTO, DeleteMaterialViewModels>();
            });
            var mapper = config.CreateMapper();

            DeleteMaterialViewModels matirialFieldOfKnowledge = mapper.Map<MatirialFieldOfKnowledgeDTO, DeleteMaterialViewModels>(adminService.FindMaterial(id));

            if (matirialFieldOfKnowledge == null)
            {
                return HttpNotFound();
            }
            return View(matirialFieldOfKnowledge);
        }

        /// <summary>
        /// Sending data to server with parameter which will delete subject
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpPost, ActionName("DeleteMaterial")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteMaterialConfirmedAsync(string id)
        {
            await adminService.DeleteMaterialAsync(id);
            return RedirectToAction("Index");
        }
        
        /// <summary>
        /// View edit subject
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ActionResult EditMaterial(string id)
        {
            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "Admin", new { mistakeName = mistakeName });
            }

            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<MatirialFieldOfKnowledgeDTO, EditMaterialViewModels>();
            });
            var mapper = config.CreateMapper();

            EditMaterialViewModels matirialFieldOfKnowledge = mapper.Map<MatirialFieldOfKnowledgeDTO, EditMaterialViewModels>(adminService.FindMaterial(id));

            if (matirialFieldOfKnowledge == null)
            {
                return HttpNotFound();
            }
            return View(matirialFieldOfKnowledge);
        }
        /// <summary>
        /// Sending data to server with parameter which will edit subject
        /// </summary>
        /// <param name="matirialFieldOfKnowledge"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMaterial(EditMaterialViewModels matirialFieldOfKnowledgeView)
        {
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<EditMaterialViewModels, MatirialFieldOfKnowledgeDTO>();
            });
            var mapper = config.CreateMapper();

            MatirialFieldOfKnowledgeDTO matirialFieldOfKnowledge = mapper.Map<EditMaterialViewModels, MatirialFieldOfKnowledgeDTO>(matirialFieldOfKnowledgeView);

            if (ModelState.IsValid)
            {
                adminService.UpdateMaterial(matirialFieldOfKnowledge);
                return RedirectToAction("Index");
            }

            return View(matirialFieldOfKnowledge);
        }

    }
}