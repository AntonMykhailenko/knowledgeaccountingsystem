﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Services;
using KnowledgeAccountingSystem.WEB.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace KnowledgeAccountingSystem.WEB.Controllers
{
    [Authorize(Roles = "admin, user")]
    public class UserController : Controller
    {
        private IUserService userService;
        private IAdminService adminService;
        private IAccountService accountService;

        /// <summary>
        /// dependency injection
        /// </summary>
        /// <param name="userServ"></param>
        /// <param name="adminServ"></param>
        /// <param name="accountServ"></param>
        public UserController(IUserService userServ,IAdminService adminServ,IAccountService accountServ)
        {
            userService = userServ;
            adminService = adminServ;
            accountService = accountServ;
        }
        /// <summary>
        /// get all fields of knowledge
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<FieldOfKnowledgeDTO, IndexViewModels>();

            });
            var mapper = config.CreateMapper();

            return View(mapper.Map<IEnumerable<FieldOfKnowledgeDTO>, IEnumerable<IndexViewModels>>(adminService.GetAll().ToList()));
        }
        /// <summary>
        /// List of subjects field of knowledges
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isUserMaterial"></param>
        /// <returns></returns>
        public ActionResult ListOfMatirialFieldOfKnowledges(string id, bool isUserMaterial=false)
        {
            ListMatirialViewModels listMatirialViewModels = new ListMatirialViewModels();

            if (isUserMaterial)
            {
                listMatirialViewModels.IsMessege = true;
                ViewBag.Message = "You have been answered, if you want you can update your knoledge";
            }

            if (id == null)
            {
                string mistakeName = "Error dont try do it";
                return RedirectToAction("Mistakes", "User", new { mistakeName = mistakeName });
            }

            if(userService.IsMaterial(id))
            {
                string mistakeName = "Material with this id is empty or absent";
                return RedirectToAction("Mistakes", "User", new { mistakeName= mistakeName });
            }

            listMatirialViewModels.Id = id;

            listMatirialViewModels.ListOfMatirial = userService.ListOfMatirialFieldOfKnowledges(id);         

            if (listMatirialViewModels.ListOfMatirial == null)
            {
                return HttpNotFound();
            }

            return View(listMatirialViewModels);
        }
        /// <summary>
        /// send answer to server
        /// </summary>
        /// <param name="idMat"></param>
        /// <param name="matirial"></param>
        /// <param name="mark"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Send(string idMat, IEnumerable<string> matirial, IEnumerable<int> mark)
        {
            
            if(mark==null || mark.Count() == 0)
            {
                string mistakeName = "Dont try do it";
                return RedirectToAction("Mistakes", "User", new { mistakeName = mistakeName });
            }
            
            foreach (var index in mark)
            {
                if (index < 1 || index > 5)
                {
                    string mistakeName = "Dont try do it";
                    return RedirectToAction("Mistakes", "User", new { mistakeName = mistakeName });
                }
            }         

            string email = HttpContext.User.Identity.Name;

            if (email == null || matirial == null || mark == null)
            {
                string mistakeName = "Material with this id is empty or absent";
                return RedirectToAction("Mistakes", "User", new { mistakeName = mistakeName });
            }

            bool isUserMaterial = userService.IsUserMaterial(email, matirial);

            if(!isUserMaterial)
            { 
                if (ModelState.IsValid)
                {
                    await userService.CreateUserAnswerAsync(email, matirial, mark);

                    return RedirectToAction("Index");
                }
            }

            ApplicationUserDTO user = await accountService.FindByName(email);

            return RedirectToAction("ListOfMatirialFieldOfKnowledges", "User", new { id= idMat,  isUserMaterial = isUserMaterial });
        }

        /// <summary>
        /// update your answer
        /// </summary>
        /// <param name="idMat"></param>
        /// <param name="matirial"></param>
        /// <param name="mark"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(string idMat, IEnumerable<string> matirial, IEnumerable<int> mark)
        {
            if(mark.Count() == 0)
            {
                string mistakeName = "Dont try do it";
                return RedirectToAction("Mistakes", "User", new { mistakeName = mistakeName });
            }

            foreach(var index in mark)
            {
                if(index < 1 || index > 5)
                {
                    string mistakeName = "Dont try do it";
                    return RedirectToAction("Mistakes", "User", new { mistakeName = mistakeName });
                }
            }
           
            string email = HttpContext.User.Identity.Name;

            if(email == null || matirial == null || mark == null)
            {
                string mistakeName = "Material with this id is empty or absent";
                return RedirectToAction("Mistakes", "User", new { mistakeName = mistakeName });
            }


            if(ModelState.IsValid)
            {
                await userService.UpdateMatirialMarksAsync(email, matirial, mark);
                return RedirectToAction("Index");
            }

            ApplicationUserDTO user = await accountService.FindByName(email);

            return RedirectToAction("ListOfMatirialFieldOfKnowledges", "User", new { id = idMat, isUserMaterial = true });
        }

        /// <summary>
        /// when something happens incorrect
        /// </summary>
        /// <param name="mistakeName"></param>
        /// <returns></returns>
        public ActionResult Mistakes(string mistakeName)
        {
            ViewBag.Messege = mistakeName;
            return View();
        }

    }
}