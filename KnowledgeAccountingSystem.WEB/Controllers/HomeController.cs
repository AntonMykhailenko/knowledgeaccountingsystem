﻿using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Services;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KnowledgeAccountingSystem.WEB.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {

        }

        /// <summary>
        /// shows view to user with his role
        /// </summary>
        /// <returns></returns>

        [AllowAnonymous]
        public ActionResult Index()
        {

            if(HttpContext.User.IsInRole("admin"))
            { 
                return RedirectToAction("Index", "Admin");
            }

            if (HttpContext.User.IsInRole("manager"))
            {
                return RedirectToAction("Index", "Manager");
            }

            if (HttpContext.User.IsInRole("user"))
            {            
                return RedirectToAction("Index", "User");
            }
           
            return View();
        }

    }
}