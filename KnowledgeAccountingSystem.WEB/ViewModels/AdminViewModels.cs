﻿using KnowledgeAccountingSystem.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeAccountingSystem.WEB.ViewModels
{
    public class IndexViewModels
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
    }

    public class ListOfMatirialFieldOfKnowledgesViewModels
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public FieldOfKnowledgeDTO FieldOfKnowledge { get; set; }
    }


    public class CreateMatirialViewModels
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string FieldOfKnowledgeId { get; set; } = Guid.NewGuid().ToString();
        public FieldOfKnowledgeDTO FieldOfKnowledge { get; set; }
        public ICollection<MarkDTO> Marks { get; set; }
        public CreateMatirialViewModels()
        {
            Marks = new List<MarkDTO>();
        }
    }

    public class CreateViewModels
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
    }

    public class EditViewModels
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
    }

    public class DeleteViewModels
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
    }

    public class WorkWithMaterial
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public FieldOfKnowledgeDTO FieldOfKnowledge { get; set; }
    }

    public class WorkWithMaterialViewModel
    {
        public IEnumerable<MatirialFieldOfKnowledgeDTO> WorkWithMaterials { get; set; }
        public PageInfoDTO PageInfo { get; set; }
    }

    public class DeleteMaterialViewModels
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public FieldOfKnowledgeDTO FieldOfKnowledge { get; set; }
    }
    //EditMaterial
    public class EditMaterialViewModels
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string FieldOfKnowledgeId { get; set; } = Guid.NewGuid().ToString();
        public FieldOfKnowledgeDTO FieldOfKnowledge { get; set; }
    }

    public class UserRolesViewModels
    {
        public ApplicationUserDTO User { get; set; }
        public IList<string> Roles { get; set; }
        public string NewRoles { get; set; }
    }

}