﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeAccountingSystem.WEB.ViewModels
{
    public class ReportViewModels
    {
        public string User { get; set; }
        public string FieldName { get; set; }
        public string MatirialName { get; set; }
        public int Score { get; set; }
    }
}