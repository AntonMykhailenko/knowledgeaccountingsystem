﻿using KnowledgeAccountingSystem.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeAccountingSystem.WEB.ViewModels
{
    public class ListMatirialViewModels
    {
        public string Id { get; set; }
        public IEnumerable<MatirialFieldOfKnowledgeDTO> ListOfMatirial { get; set; }
        public bool IsMessege { get; set; }
    }
}